require_relative 'operacoes_extras'
require 'net/http'
require 'json'

      $op1 =0 #variável que conta a quantidade de vezes que você acessou a operação 1
      $op2 =0 #variável que conta a quantidade de vezes que você acessou a operação 2
      $op3 =0 #variável que conta a quantidade de vezes que você acessou a operação 3

module Calculadora

  class Operacoes
    include OperacoesExtras

    def media_preconceituosa(notas, lista_negra)
      json_notas=JSON.parse(notas) #Transforma o string "notas" em um hash
      array_lista_negra=lista_negra.split(" ") #Separa o string "lista_negra", a cada espaço " ", em um vetor

      notas_sem_lista_negra = json_notas.select do |k,v|      #Passa os alunos selecionados pela condição para "notas_sem_lista_negra"
                            not array_lista_negra.include? k  #Condição responsável por selecionar os alunos que não estão em "array_lista_negra"
      end
      
      soma=0                                        
      array_soma=notas_sem_lista_negra.map do |k,v| #Passa para array_soma a soma das notas dos alunos
        soma=soma+v
      end
      soma = array_soma.last #Como a cada termo de "array_soma" é passada a soma de uma nota com a próxma, o último termo será a soma total

      media = soma.to_f/notas_sem_lista_negra.length #Cálculo da média
      puts ""
      puts "A média calculada é igual a #{media}."
      puts "" 

    end
    
    def sem_numeros(numeros)
      array_numeros = numeros.split(" ") #Separa o string "array_numeros", a cada espaço " ", em um vetor

      array_numeros.map! do |n| #Rescreve os próprios termos a partir das operações a seguir
        if n=="0" #O algoritmo considera o string "0" diferente de "00"
          n="00"  #Como "00" é mais abrangente que "0", sempre que tivermos "0", o algoritmo o converte em "00"
        else
          n=n
        end
      end
      array_numeros.map! do |n| #Rescreve os próprios termos transformando-os e "S" ou "N"
        if (n[n.length-2..-1].include?"00") || (n[n.length-2..-1].include?"25") || (n[n.length-2..-1].include?"50") || (n[n.length-2..-1].include?"75")
          n = "S" #A linha acima testa se os dois últimos números do string incluem 0, 25, 50 ou 75
        else 
          n = "N"
        end
      end
      array_numeros.each do |n|
      print "#{n} " #Imprime na tela os termos do vetor, agora transformados em "S" ou "N"
      end
      puts " "
    end

    def filtrar_filmes(generos, ano)
      filmes = get_filmes #Atribui a "filmes" a API 
      json_generos=JSON.parse(generos) #Tranforma o vetor generos em um hash
      filmes_generos_totais=[] #Inicializando o vetor "filmes_genros_totais" como vazio, ele inclui os nomes dos filmes, já filtrados por data e genero

      w=0
      while w<json_generos.length #Percorre o loop uma quantidade de vezes igual a quantidade de generos escolhidos como filtro
      filmes_generos = filmes[:movies].select do |k, v| #Passa para "filmes_generos" os filmes filtrados por genero
                       k[:genres].include?json_generos[w] #Condição responsável por identificar se o filme possui o genero especificado
      end                                                 #OBS: Cada vez que ele rodar essa parte ele estará procurando por um genero diferente
      filmes_generos_ano = filmes_generos.select do |k,v| #Passa para "filmes_generos_ano" os filmes lançados a partir da data especificada
                           k[:year].to_i>=ano             #Identifica se o filme foi lançado a partir do ano limite
      end
      k=0
      while k< filmes_generos_ano.length                  #Percorre o loop uma quantidade de vezes igual a quantidade de filmes remanescentes depois do filtro
        if not filmes_generos_totais.include?filmes_generos_ano[k][:title] #Identifica se o filme a ser adicionado a "filmes_generos_totais" é repetido
      filmes_generos_totais.push(filmes_generos_ano[k][:title]) #Adiciona os novos filmes filtrados 
      end
      k=k+1
    end
      w=w+1
    end      #Aqui termina o primeiro loop, após isso ele irá filtrar os filmes pelo próximo gênero especificado pelo usuário, que mais tarde serão adicionados em "filmes_generos_ano" se não forem repetidos
    if filmes_generos_totais.length == 0 #Inprime uma menssagem se nenhum filme correpondente ao filtro tiver sido identificado
      puts ""
      puts "Nenhum filme econtrado com as especificações dadas. :/"
      puts ""
    else
      puts ""
      puts "Filmes lançados apartir de #{ano}, relacionados aos generos digitados: "
      puts ""
      filmes_generos_totais.each do |k| #imprime os filmes correspondentes ao filtro
      puts k
      end
      end
    end
    
    private

    def get_filmes
      url = 'https://raw.githubusercontent.com/yegor-sytnyk/movies-list/master/db.json'
      uri = URI(url)
      response = Net::HTTP.get(uri)
      return JSON.parse(response, symbolize_names: true)
    end

  end

  class Menu
    include OperacoesExtras #Inclui o módulo "OperacoesExtras" criado no arquivo "operacoes_Extras.rb"
    def initialize
      puts " -----------------------------------" #MENU DO ISRAEL
      puts "| Bem vindo a calculadora do Israel |"
      puts " -----------------------------------"
      puts ""
      puts "  Opção | Operação correspondente"
      puts "  ==============================="
      puts "    1   |  Média preconceituosa"
      puts "    2   | Calculadora sem números"
      puts "    3   |     Filtrar filmes"
      puts "    4   |       Histórico"
      puts "    5   |          Sair"
      puts ""
      print "Digite sua opção: "
      opcao = 0
      opcao = gets.chomp.to_i #Armazena a opção escolhida
      puts ""
      if opcao == 1 #Testa se a opção escolhida foi a primeira
        puts "Você escolheu média preconceituosa"
        opcao=1
        while opcao ==1 
        puts "Digite o nome do aluno acompanhado de sua nota, seguindo o exemplo:"
        puts ""
        puts "-EXEMPLO:"
        puts "{'Aluno1':NotaDoAluno1, 'Aluno2':NotaDoAluno2, 'Aluno3':NotaDoAluno3}"
        puts ""
        notas = gets.chomp #Armazena o string que mais tarde será transformado em um hash
        opcao =0
        while opcao==0 #Inicia um loop que se repetirá se o usuário inserir uma opção inválida
          opcao = 0
        puts "Gostaria de adicionar uma lista negra?"
        puts ""
        puts "  Opção | SIM/NÃO"
        puts "  ==============="
        puts "    1   |  Sim"
        puts "    2   |  Não"
        puts ""
        print "Digite sua opção: "
        opcao = gets.chomp.to_i #Armazena a reposta do usuário
        puts ""
        if opcao==1
          puts "Você escolheu: Sim"
          puts ""
          puts "Digite os nomes dos alunos a serem excluídos do cálculo da média seguindo o exemplo:"
          puts ""
          puts "-EXEMPLO:"
          puts "Aluno1 Aluno2 Aluno3"
          puts ""
          lista_negra = gets.chomp #Armazena a lista negra
          Operacoes.new.media_preconceituosa(notas,lista_negra) #Chama o método "media_preconceituosa" e passa os parâmetros necessários
          $op1 = $op1+1 #Adiciona +1 a variável responsável por identificar quantas o usuário acessou essa função
          opcao = 0
          while opcao == 0 #Inicia o loop que se repetirá se o usuário inserir uma resposta inválida
            opcao = 0
            puts "Gostaria de efetura outro cálculo de média?"
            puts ""
            puts "  Opção | SIM/NÃO"
            puts "  ==============="
            puts "    1   |  Sim"
            puts "    2   |  Não"
            puts ""
            print "Digite sua opção: "
            opcao = gets.chomp.to_i
            puts ""
            if opcao == 1
              puts "Você escolheu: Sim"
              puts ""
              elsif opcao ==2
                puts "Você escolheu: Não"
                puts ""
                initialize #Volta para o MENU DO ISRAEL
              else
                puts "Opção inválida. Tente novamente."
                opcao=0
            end
          end
        elsif opcao==2 #A partir deste ponto o algoritmo se torna bastante repetitivo
          puts "Você escolheu: Não"
          lista_negra = []
          lista_negra = ""
          Operacoes.new.media_preconceituosa(notas,lista_negra)
          $op1 =$op1+1
          opcao = 0
          while opcao == 0
            opcao = 0
            puts "Gostaria de efetura outro cálculo de média?"
            puts ""
            puts "  Opção | SIM/NÃO"
            puts "  ==============="
            puts "    1   |  Sim"
            puts "    2   |  Não"
            puts ""
            print "Digite sua opção: "
            opcao = gets.chomp.to_i
            puts ""
            if opcao == 1
              puts "Você escolheu: Sim"
              puts ""
              elsif opcao ==2
                puts "Você escolheu: Não"
                puts ""
                initialize
              else
                puts "Opção inválida. Tente novamente."
                opcao=0
            end
          end
        else
        puts "Opção inválida. Tente novamente."
          opcao=0
        end
        end
      end
      elsif opcao ==2
        puts "Você escolheu calculadora sem números"
        opcao = 1
        while opcao == 1
        puts ""
        puts "Digite os números a serem testados em relação a divisbilidade por 25 seguindo o exemplo."
        puts ""
        puts "-EXEMPLO:"
        puts "Num1 Num2 Num3"
        numeros = gets.chomp.to_s
        
        Operacoes.new.sem_numeros(numeros)
        $op2=$op2+1
        opcao = 0
        while opcao == 0
          puts "Gostaria de efetura outro teste de divisibilidade?"
          puts ""
          puts "  Opção | SIM/NÃO"
          puts "  ==============="
          puts "    1   |  Sim"
          puts "    2   |  Não"
          puts ""
          print "Digite sua opção: "
          opcao = gets.chomp.to_i
          puts ""
          if opcao == 1
            puts "Você escolheu: Sim"
            puts ""
            elsif opcao ==2
              puts "Você escolheu: Não"
              puts ""
              initialize
            else
              puts "Opção inválida. Tente novamente."
              opcao=0
          end
        end
      end
      elsif opcao ==3
        puts "Você escolheu filtrar filmes"
        opcao =1
        while opcao ==1
        puts ""
        puts "Digite os genêros de filmes a aparecerem na pesquisa seguindo o exemplo, por favor digitar em inglês."
        puts ""
        puts "-EXEMPLO"
        puts "['Genero1', 'Genero2', 'Genero3']"
        puts ""
        generos = gets.chomp
        puts ""
        puts "Digite a partir de que data de lançamento os filmes devem aparecer na pesquisa."
        puts ""
        ano = gets.chomp.to_i
        Operacoes.new.filtrar_filmes(generos,ano)
        $op3=$op3+1
        opcao = 0
        while opcao == 0
          puts "Gostaria de realizar outra filtragem de filmes?"
          puts ""
          puts "  Opção | SIM/NÃO"
          puts "  ==============="
          puts "    1   |  Sim"
          puts "    2   |  Não"
          puts ""
          print "Digite sua opção: "
          opcao = gets.chomp.to_i
          puts ""
          if opcao == 1
            puts "Você escolheu: Sim"
            puts ""
            elsif opcao ==2
              puts "Você escolheu: Não"
              puts ""
              initialize
            else
              puts "Opção inválida. Tente novamente."
              opcao=0
          end
        end
      end
      elsif opcao == 4
        puts "Você escolheu o Histórico"
        puts ""
        Operacao_Extra.new.historico($op1,$op2,$op3)
      elsif opcao == 5
        puts "Obrigado por ultilizar a calculadora do Israel"
        puts "              #GTIVAIQUEVAI"
        exit #Responsável por finalizar o programa
      else
        puts "Opção Inválida"
      end

    end
  
  end

end
